import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UserManual04Component } from './user-manual04.component';

describe('UserManual04Component', () => {
  let component: UserManual04Component;
  let fixture: ComponentFixture<UserManual04Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [UserManual04Component]
    })
    .compileComponents();

    fixture = TestBed.createComponent(UserManual04Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

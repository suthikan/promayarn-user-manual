import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UserManual01Component } from './user-manual-01.component';

describe('UserManual01Component', () => {
  let component: UserManual01Component;
  let fixture: ComponentFixture<UserManual01Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [UserManual01Component]
    })
    .compileComponents();

    fixture = TestBed.createComponent(UserManual01Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

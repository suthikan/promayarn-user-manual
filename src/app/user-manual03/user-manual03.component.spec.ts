import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UserManual03Component } from './user-manual03.component';

describe('UserManual03Component', () => {
  let component: UserManual03Component;
  let fixture: ComponentFixture<UserManual03Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [UserManual03Component]
    })
    .compileComponents();

    fixture = TestBed.createComponent(UserManual03Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

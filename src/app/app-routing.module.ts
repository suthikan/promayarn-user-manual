import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { UserManualComponent } from './user-manual/user-manual.component';
import { UserManual01Component } from './user-manual-01/user-manual-01.component';
import { UserManual03Component } from './user-manual03/user-manual03.component';
import { UserManual02Component } from './user-manual02/user-manual02.component';
import { UserManual04Component } from './user-manual04/user-manual04.component';
import { UserManual05Component } from './user-manual05/user-manual05.component';

const routes: Routes = [

  {
    path: 'UserManual', component: AppComponent,
    children: [
      {
        path: '', component: UserManualComponent
      },
      {
        path: 'UserManual01', component: UserManual01Component
      },
      {
        path: 'UserManual02', component: UserManual02Component
      },
      {
        path: 'UserManual03', component: UserManual03Component
      },
      {
        path: 'UserManual04', component: UserManual04Component
      },
      {
        path: 'UserManual05', component: UserManual05Component
      },
    ]
  },
  {
    path: "**",
    redirectTo: "UserManual",
    pathMatch: "full",
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

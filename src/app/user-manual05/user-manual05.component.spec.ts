import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UserManual05Component } from './user-manual05.component';

describe('UserManual05Component', () => {
  let component: UserManual05Component;
  let fixture: ComponentFixture<UserManual05Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [UserManual05Component]
    })
    .compileComponents();

    fixture = TestBed.createComponent(UserManual05Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

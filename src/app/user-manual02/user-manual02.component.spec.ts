import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UserManual02Component } from './user-manual02.component';

describe('UserManual02Component', () => {
  let component: UserManual02Component;
  let fixture: ComponentFixture<UserManual02Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [UserManual02Component]
    })
    .compileComponents();

    fixture = TestBed.createComponent(UserManual02Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
